<div class="row">
    <div class="column">
        <div class="users form content">
    <?= $this->Flash->render() ?>
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Please enter your email and password') ?></legend>
        <?= $this->Form->control('email', ['required' => true]) ?>
        <?= $this->Form->control('password', ['required' => true]) ?>
    </fieldset>
    <?= $this->Form->submit(__('Login'), ['class' => 'button']); ?>
    <?= $this->Form->end() ?>
     <?= $this->Html->link(__('Or create account'), ['controller' => 'Users', 'action' => 'add'])?>
</div>

    </div>
</div>
