<?php
?>

<main class="content card">
<div class="grid-x thanks">
	<div class="cell large-12">
		<p><?= $this->Html->image('check.svg', ['class' => 'check']) ?></p>
		<h3>
			Your Account has been created
		</h3>
		<h5>
			Congratulation on Joining Our Community of Positive Changes and Beliefs
		</h5>
	</div>
</div>


<div class="grid-x thanks">
	<div class="cell large-12">
		<p style="margin-top: 2rem;">
			Lorem ipsum dolor, sit amet consectetur adipisicing elit. Animi a pariatur laborum quasi quam, tenetur voluptatibus ea vel maiores culpa. Maxime, quis autem odio reiciendis, ex, nobis explicabo illo earum ullam exercitationem debitis voluptatibus laboriosam. Quo accusamus error sequi odio facere sint accusantium dolore, non expedita asperiores rem, facilis, quas dolores neque eos, voluptates. Maiores, inventore.
		</p>
		<p><?= $this->Html->link(__('MORE AFFIRMATIONS'), ['controller' => 'Articles', 'action'=>'random'] , ['class' => 'button']) ?></p>
		
	</div>
</div>

</main>
