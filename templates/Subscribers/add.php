<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Subscriber $subscriber
 */
?>
<div id = "subscribers-add" class="content">
    <div class="grid-x ">
        <div class="cell large-12 ">
            <h1 style="font-weight:bold;text-align: center; color: var(--primary-color);">
                <?= __('Affirm Yourself Once A Day')?>
            </h1>
            <h4 style="text-align: center; font-style: italic;">
                <?= __('Change your mood, state of mind, and manifest the change you desire in your life - FREE')?>
            </h4>
        </div>
    </div>

    <div class="grid-x">
        <div  class="cell large-12 card card-rounded">
            
            <p>
                Join Our Community of 28,753 like-minded members and help yourself to challenge and overcome self-sabotaging and negative thought every single day
            </p>
        </div>
    </div>

    <?= $this->Form->create($subscriber); ?>
    <div  class="grid-x card-cta">
        <div class="cell medium-6">
            <?= $this->Form->control('name', ['label' => false,'placeholder' => 'Your Name', 'class' => 'custom-input-sub']);?> 
        </div>

        <div class="cell medium-6">
            <?= $this->Form->control('email', ['label' => false, 'placeholder' => 'Your Email', 'class' => 'custom-input-sub']);?> 
           
        </div>

    </div>

    <div class="grid-x">
       <div class="cell ">
           <?= $this->Form->button(__('Subscribe to Affirms - FREE'), ['class' => 'large button button-shadow', 'style' => 'width:inherit']) ?>
           <?= $this->Form->end() ?>
       </div>
    </div>
</div>