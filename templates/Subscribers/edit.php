<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Subscriber $subscriber
 */
?>
<div class="row">
    
    <div class="column-responsive column-80">
        <div class="subscribers form content">
            <?= $this->Form->create($subscriber) ?>
            <fieldset>
                <legend><?= __('Edit Subscriber') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('email');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
