<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$siteDescription = 'Affirms: Positive energy for happier life';
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $siteDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css('foundation.min');?>
    <?= $this->Html->css('app');?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar topbar-responsive">
  <div class="top-bar-title">
    <span data-responsive-toggle="topbar-responsive" data-hide-for="medium">
      <button class="menu-icon" type="button" data-toggle></button>
    </span>
   
         <a class="topbar-responsive-logo" href="<?= $this->Url->build('/') ?>"><strong><?= $this->Html->image('logo.png',['class' => 'logo']) ?></strong></a> 
    
    <!-- <a class="topbar-responsive-logo" href="#"><strong>DailyUp</strong></a>  -->

  </div>
  <div id="topbar-responsive" class="topbar-responsive-links">
    <div class="top-bar-right">
      <ul class="menu simple vertical medium-horizontal">
        
        <li>
         <!--  <button type="button" class="button hollow topbar-responsive-button">Join Us</button> -->
         <button type="button" class="button hollow topbar-responsive-button">
             <a href="
             <?= $this->Url->build([
            'controller' => 'Subscribers',
            'action' => 'add']);
         ?>">
     Join Us</a>
         

         </button>
        </li>
      </ul>
    </div>
  </div>
</nav>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
    </footer>
    <?= $this->Html->script('jquery.min.js')?>
    <?= $this->Html->script('what-input.min.js')?>
    <?= $this->Html->script('foundation.min.js')?>   
    <script>
          $(document).foundation();
    </script>
</body>

<footer id="footer-content">
        <p> &copy; 2021  All Rights Reserved. 
            <?= $this->Html->link(__('Terms'), ['controller' =>'Pages','action' => 'display', 'terms'])?> 
            <?= $this->Html->link(__('Privacy'), ['controller' =>'Pages','action'=>'display', 'privacy'])?> 
        </p>
</footer> 




</html>
