<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.10.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->disableAutoLayout();

$checkConnection = function (string $name) {
    $error = null;
    $connected = false;
    try {
        $connection = ConnectionManager::get($name);
        $connected = $connection->connect();
    } catch (Exception $connectionError) {
        $error = $connectionError->getMessage();
        if (method_exists($connectionError, 'getAttributes')) {
            $attributes = $connectionError->getAttributes();
            if (isset($attributes['message'])) {
                $error .= '<br />' . $attributes['message'];
            }
        }
    }

    return compact('connected', 'error');
};



$siteDescription = 'Affirms: Positive energy for happier life';
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $siteDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    
    <?= $this->Html->css('foundation.min');?>
    <?= $this->Html->css('app');?>
    <?= $this->Html->css('https://cdn.jsdelivr.net/npm/motion-ui@1.2.3/dist/motion-ui.min.css'); ?>
   

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

<nav class="top-bar topbar-responsive">
  <div class="top-bar-title">
    <span data-responsive-toggle="topbar-responsive" data-hide-for="medium">
      <button class="menu-icon" type="button" data-toggle></button>
    </span>
    <a class="topbar-responsive-logo" href="#"><strong><?= $this->Html->image('logo.png',['class' => 'logo']) ?></strong></a> 

  </div>
  <div id="topbar-responsive" class="topbar-responsive-links">
    <div class="top-bar-right">
      <ul class="menu simple vertical medium-horizontal">
        
        <li>
         <!--  <button type="button" class="button hollow topbar-responsive-button">Join Us</button> -->
         <button type="button" class="button hollow topbar-responsive-button">
             <a href="
             <?= $this->Url->build([
            'controller' => 'Subscribers',
            'action' => 'add']);
         ?>">
     Join Us</a>
         

         </button>
        </li>
      </ul>
    </div>
  </div>
</nav>

<main class="content">
    <div class="grid-x">
        <div class="cell large-6">
            <h1 class="promo-hero-title" style="font-weight:bold;"><?= __('Welcome to Your Daily Affirmations')?></h1>

            <hr class ="hr">
            <p class="cta">
                <?= __('Learn something new everyday with today\'s featured affirmation')?>
            </p>
            <p class="cta"> <?= $this->Html->link(__('CLICK HERE'), ['controller' => 'Articles', 'action' => 'random'], [
                'class' => 'button large medium-expanded lg-btn button-shadow',
                ]) ?>
                    
                </p>
        </div>
        <div class="cell large-6">
            <?= $this->Html->image('bookgif.gif',['class' => 'magic-book']); ?>
        </div>
    </div>

</main>

<div class="grid-x">
    <div class="promo-hero">
        <div class="promo-hero-content">
            <h1 class="promo-hero-title">Welcome to Your Daily Affirmations</h1>
            <p class="promo-hero-description hide-for-small-only">There are many ways to use affirmations as a way of startin your day. Whether they are for reflectin on whay you've accomplished, settin an intention for the day, or simply to start your day with positivity, there's no better way to feel empowered and inspired than by using affirmations.</p>
            <div class="promo-hero-ctas">
              <?= $this->Html->link(__('Get your affirmation now'), ['controller' => 'Articles', 'action' => 'random'], 
              ['class' => 'button large medium-expanded button-shadow ']) ?>
            </div>
        </div>
        
    </div>
</div>

<div id="testimonial">
    <div class="grid-x ">
        <div class="cell ">
            <div class="testimonial-content">
                <h2>Check Out What Others Are Saying</h2>
                <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                  <div class="orbit-wrapper">
                    <!-- <div class="orbit-controls">
                      <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
                      <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
                    </div> -->
                    <ul class="orbit-container">
                      <li class="is-active orbit-slide">
                        <figure class="orbit-figure">
                          
                          <h4>John Mason</h4>
                          
                            <p>Really loved the timely affirmations.Very uplifting</p>
                          
                        </figure>
                      </li>
                      <li class="orbit-slide">
                        <figure class="orbit-figure">
                            <h4>Queen Washington</h4>
                          <p>Since I started reading the affirmations, I always come back on the site to check what's new</p>
                          
                        </figure>
                      </li>
                      <li class="orbit-slide">
                        <figure class="orbit-figure">
                            <h4>Kate Drewmore</h4>
                          <p>
                              I love getting my personal emails in my inbox. Very timely and I highly recommend to anyone
                          </p>
                          
                        </figure>
                      </li>
                      <li class="orbit-slide">
                        <figure class="orbit-figure">
                            <h4>Matt</h4>
                          <p>Lorem ipsum dolor sit, amet consectetur, adipisicing elit. Incidunt distinctio nemo modi nam, corrupti ad adipisci, eum beatae in, necessitatibus quidem? Esse voluptatum eum reprehenderit fugiat atque cupiditate! Illo libero facilis est excepturi, tempore totam saepe animi. Provident, id fugit! Delectus rem error distinctio.</p>
                          
                        </figure>
                      </li>
                    </ul>
                  </div>
                  <nav class="orbit-bullets">
                    <button class="is-active" data-slide="0">
                      <span class="show-for-sr">First slide details.</span>
                      <span class="show-for-sr" data-slide-active-label>Current Slide</span>
                    </button>
                    <button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
                    <button data-slide="2"><span class="show-for-sr">Third slide details.</span></button>
                    <button data-slide="3"><span class="show-for-sr">Fourth slide details.</span></button>
                  </nav>
                </div>
            </div>

            

        </div>
    </div>
    <div class="grid-x">
        <div class="cell content">
            
            
            
        </div>
    </div>
    
</div> 








<script src="js/jquery.min.js"></script>
<script src="js/what-input.min.js"></script>
<script src="js/foundation.min.js"></script>    
<script>
      $(document).foundation();
</script>


</body>

    <footer>
        <p> &copy; 2021  All Rights Reserved. 
            <?= $this->Html->link(__('Terms'), ['controller' =>'Pages','terms'])?> 
            <?= $this->Html->link(__('Privacy'), ['controller' =>'Pages','privacy'])?> 
        </p>
        
    </footer>

</html>
