<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.10.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->disableAutoLayout();

$checkConnection = function (string $name) {
    $error = null;
    $connected = false;
    try {
        $connection = ConnectionManager::get($name);
        $connected = $connection->connect();
    } catch (Exception $connectionError) {
        $error = $connectionError->getMessage();
        if (method_exists($connectionError, 'getAttributes')) {
            $attributes = $connectionError->getAttributes();
            if (isset($attributes['message'])) {
                $error .= '<br />' . $attributes['message'];
            }
        }
    }

    return compact('connected', 'error');
};



$siteDescription = 'Affirms: Positive energy for happier life';
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $siteDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    
    <?= $this->Html->css('foundation.min');?>
    <?= $this->Html->css('app');?>

    
   

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

<nav class="top-bar topbar-responsive">
  <div class="top-bar-title">
    <span data-responsive-toggle="topbar-responsive" data-hide-for="medium">
      <button class="menu-icon" type="button" data-toggle></button>
    </span>
    <a class="topbar-responsive-logo" href="<?= $this->Url->build('/') ?>"><strong><?= $this->Html->image('logo.png',['class' => 'logo']) ?></strong></a> 

  </div>
  <div id="topbar-responsive" class="topbar-responsive-links">
    <div class="top-bar-right">
      <ul class="menu simple vertical medium-horizontal">
        
        <li>
         <!--  <button type="button" class="button hollow topbar-responsive-button">Join Us</button> -->
         <button type="button" class="button hollow topbar-responsive-button">
             <a href="
             <?= $this->Url->build([
            'controller' => 'Subscribers',
            'action' => 'add']);
         ?>">
     Join Us</a>
         

         </button>
        </li>
      </ul>
    </div>
  </div>
</nav>

<main class="content">
    <div class="grid-x">
        <div class="cell">
            <h1>Terms</h1>
            <p>Lorem ipsum dolor sit amet, consectetur, adipisicing elit. Earum possimus doloribus ratione hic, provident unde, impedit, saepe delectus sit, quibusdam inventore ipsa velit. Sed quis dicta deserunt culpa nam omnis dolorem eos, labore pariatur, modi fugiat laudantium consectetur quibusdam consequatur ab aliquid incidunt dolor dignissimos, perspiciatis, natus accusantium officiis tempore? Eius dolores qui, numquam, animi nesciunt laborum quae repellendus illo veritatis quia itaque esse sit, vel totam magnam ipsam, inventore blanditiis ex nostrum in id aperiam asperiores ullam fuga. Voluptates ducimus tenetur corrupti adipisci cum repudiandae debitis beatae error veritatis qui neque nulla eos sapiente tempora animi, suscipit culpa libero accusantium modi necessitatibus enim dolore repellat! Exercitationem possimus delectus distinctio porro beatae ducimus magni, quas eius corporis sunt fuga nihil officia praesentium excepturi repellendus assumenda quod quidem iure ad, vitae quia voluptatem minima sint, optio! Pariatur consectetur laudantium, dolore, iusto dolorum ab corrupti aliquid at. Quo ducimus totam doloremque perspiciatis.</p>
        </div>
    </div>

</main>



<script src="js/jquery.min.js"></script>
<script src="js/what-input.min.js"></script>
<script src="js/foundation.min.js"></script>    
<script>
      $(document).foundation();
</script>
</body>

    <footer>
        <p> &copy; 2021  All Rights Reserved. 
            <?= $this->Html->link(__('Terms'), ['controller' =>'Pages','terms'])?> 
            <?= $this->Html->link(__('Privacy'), ['controller' =>'Pages','privacy'])?> 
        </p>
        
    </footer>

</html>
