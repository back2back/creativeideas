<?php
echo $this->Html->css('social-share-kit');
echo $this->Html->script('social-share-kit.min', ['block' => true]);
echo $this->fetch('css') ;
echo $this->fetch('js') ;
?>
<main class="content">
	<div class="grid-x">
		<div id="affirmation" class="cell medium-8 card">
			
			<h4 class="date-header" style="font-weight:bold"> <?= $date?> </h4>

			<h3 style="font-weight:bold; font-style: normal;" > Affirmation of The Day </h3>
			
			<h3 style="font-weight:bold" ><q><?= $data->title ?></q></h3>
			<p>
				<?=$data->description?>
			</p>

			
			<?php
		  $url =
		  $this->Url->build([
            'controller' => 'Articles',
            'action' => 'view', $data->id], ['fullBase' => true]);
         
		?>
		<div class="cell medium-12">
			<div class="ssk-group">
				<h4>Share:</h4>
			    <a href="" class="ssk ssk-facebook"></a>
			    <a href="" class="ssk ssk-twitter"></a>
			    
			</div>
		</div>

		<div id="resource" class="grid-x">
			<div class="cell">
				<h2>Resource for the Day</h2>
				<p>
					Check out our carefully sourced Resource of the Day
				</p>
			</br>
				<a href="https://www.aweber.com/" class="button large medium-expanded lg-btn button-shadow">Check it out</a>
				</br>
				<?= $this->Html->link(__('Read Another'),
				    	['controller' => 'Articles','action' => 'random'],
				    	['class' => 'button large medium-expanded lg-btn button-shadow'])?>
			</div>
	</div>



		</div>
		<div id="side-random" class="cell medium-4 right" data-sticky-container>
			<!-- <div class="sticky" data-sticky data-anchor="affirmation"> -->
				<div class="sticky" data-sticky data-margin-top=0 >
			    <?= $this->Html->image('bookgif.gif')?>
			    <p>
			    	<?= $this->Html->link(__('Surprise me with another affirmation'),
			    	['controller' => 'Articles','action' => 'random'],
			    	['class' => 'button'])?>
			    </p>
			</div>
		</div>
	</div>
	
</main>


<script type="text/javascript">
	SocialShareKit.init({
		url: '<?= $url?>'
	});
</script>
